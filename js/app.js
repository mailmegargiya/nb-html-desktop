function isTouchDevice() {
    return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}
if (isTouchDevice() === true) {
    //your logic for touch device here
    // Category Dropdown tabs
    $('a[data-type=tabs]').click(function (event) {
        event.stopPropagation();
        if (!$(this).hasClass('active')) {
            var current = $(this).attr('data-target');
            $(this).parent().parent('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $('[data-type=tabContent]').removeClass('active');
            $(current).addClass('active');
        }
    });
    // Brand Dropdown tabs
    $('a[data-type=brandSort]').click(function (event) {
        event.stopPropagation();
        if (!$(this).hasClass('active')) {
            var current = $(this).attr('data-target');
            $(this).parent().parent('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $('[data-type=brandContent]').removeClass('active');
            $(current).addClass('active');
        }
    });
} else {
    //your logic for non touch device here
    // Nav hover event insted of click
    $('.nav-item.dropdown').hover(function () {
            $(this).addClass('show');
            $(this).find('.dropdown-menu').addClass('show');
        },
        function () {
            $(this).removeClass('show');
            $(this).find('.dropdown-menu').removeClass('show');
        });
    // Category Dropdown tabs
    $('a[data-type=tabs]').hover(function (event) {
        event.stopPropagation();
        if (!$(this).hasClass('active')) {
            var current = $(this).attr('data-target');
            $(this).parent().parent('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $('[data-type=tabContent]').removeClass('active');
            $(current).addClass('active');
        }
    });
    // Brand Dropdown tabs
    $('a[data-type=brandSort]').hover(function (event) {
        event.stopPropagation();
        if (!$(this).hasClass('active')) {
            var current = $(this).attr('data-target');
            $(this).parent().parent('ul').find('a').removeClass('active');
            $(this).addClass('active');
            $('[data-type=brandContent]').removeClass('active');
            $(current).addClass('active');
        }
    });
}

// Brand Search Filter
$("#brandSearch").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $(".slider-content ul ul li, .slider-content > ul > li > a, .slider-nav a").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    }); 
    $(".slider-nav a").filter(function () {
        $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});

$('#brandDropdown').hover(function () {
    $('#slider, .brandShow').css('max-height', $(window).height() - 130 + "px");
    setTimeout(function () {
        $('.slider-nav').filter(function () {
            return this;
        }).slice(0, 1).remove();
        $('#slider').sliderNav({
            arrows: false
        });
        $('.slider-nav a').click(function (event) {
            event.stopPropagation();
        });
    }, 10);
});


$("#filterBrandSearch").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $(".brandSelect .custom-checkbox").filter(function () {
        $(this).toggle($(this).find('label').text().toLowerCase().indexOf(value) > -1)
    });
});

// Carousel
$('.slide4').slick({
    dots: false,
    infinite: true,
    arrows: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [{
        breakpoint: 1300,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3
        }
    }, {
        breakpoint: 1191,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    }]
});
$('.slide5').slick({
    dots: false,
    infinite: true,
    arrows: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [{
        breakpoint: 1300,
        settings: {
            slidesToShow: 4,
            slidesToScroll: 4
        }
    }, {
        breakpoint: 1191,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3
        }
    }]
});

// Top Of Page
$(".topOfPage").click(function () {
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
});

// selected remove on click
$('.selectedFilters span').click(function () {
    $(this).remove();
});

// Caterogy Select
$('ul.tree a').click(function () {
    $(this).parents('.tree').find('a').removeClass('active');
    $(this).addClass('active');
});

// Product Page
//Slick slider initialize


$('.imageGallery').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-thumbs'
});
$('.slider-thumbs').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.imageGallery',
    centerMode: true,
    focusOnSelect: true,
    variableWidth: true
});

// Choose product
$('.selectWeight label').click(function () {
    var current = $(this).find('span').text();
    $(this).parents('.weight').find('h4').find('span').text(current);
});
$('.seeMore').click(function () {
    $(this).toggleClass('active').prev().toggleClass('height');
    $(this).find('span').text(function (i, text) {
        return text === "More options" ? "Less options" : "More options";
    })
});
$('.readMore').click(function () {
    $(this).toggleClass('active').parent().toggleClass('heightAuto');
    $(this).find('span').text(function (i, text) {
        return text === "Read More" ? "Read Less" : "Read More";
    })
});
// proDetailTabs
$('#proDetailTabs a').click(function () {
    if (!$(this).hasClass('active')) {
        var current = $(this).attr('data-target');
        $('#proDetailTabs a').removeClass('active');
        $(this).addClass('active');
        $('.targetContent').slideUp();
        $(current).slideDown();
    }
});
// starPercent 
$('.starPercent').each(function () {
    var percent = $(this).next('.percent').text();
    $(this).find('span').css('width', percent);
});

// Qty
$('.minus').click(function () {
    var val = parseInt($(this).next('span').text());
    if (val > 1) {
        $(this).next('span').text(val - 1);
    }
});
$('.plus').click(function () {
    var val = parseInt($(this).prev('span').text());
    $(this).prev('span').text(val + 1);
});
// Promocode Dropdown
$('.applyPromocode button').click(function () {
    $(this).toggleClass('active');
    $('.selectPromocode').slideToggle();
});
$('.selectPromocode input[type="radio"]').click(function () {
    if ($(this).is(':checked')) {
        $(this).parents('.selectPromocode').siblings('.promocode').val($(this).next().find('.pCode').text()).parent().addClass('promocodeAdded');

    }
});
$('.applyPromocode .remove').click(function () {
    $(this).siblings('.promocode').val("");
    $('.selectPromocode input').prop('checked', false);
    $(this).hide();
});
// checkoutLoginPass
function checkoutLoginPass(el) {
    $(el).parent('.email').hide().next('.password').fadeIn();
}
$('[data-target=selectDropdown] .dropdown-item').click(function (event) {
    var current = $(this).text();
    $(this).parents('.dropdown').find('.dropdown-toggle').find('.value').text(current);
});

// offersTabList 
$('#offersTabList a').click(function(){
    var current = $(this).attr('data-target');
    $('#offersTabList a').removeClass('active');
    $(this).addClass('active');
    $('.offerTabContent').removeClass('active');
    $(current).addClass('active');
});

// Sort By
$('.sortBy a').click(function(){
    $('.sortBy a').removeClass('active');
    $(this).addClass('active');
});

// Count Down
function countDownTimer(date) {
    var elem = $('.countDownTimer');
    var futureTime = new Date(date).getTime();
    setInterval(function () {
        var timeLeft = Math.floor((futureTime - new Date().getTime()) / 1000);
        var days = Math.floor(timeLeft / 86400);
        timeLeft -= days * 86400;
        var hours = Math.floor(timeLeft / 3600) % 24;
        timeLeft -= hours * 3600;
        var min = Math.floor(timeLeft / 60) % 60;
        timeLeft -= min * 60;
        var sec = timeLeft % 60;
        var timeString = "<span class='days'>" + days + "d " + "</span>" +
            "<span class='hours'>" + hours + "h " + "</span>" +
            "<span class='minutes'>" + min + "m " + "</span>" +
            "<span class='seconds'>" + sec + "s " + "</span>";

        elem.html(timeString);
    }, 1000);
}

// Enter date in this format: January 1, 2017 12:00:00
countDownTimer('Febuary 10, 2019 12:00:00');

// Sale CountDown
function makeTimer() {
    //		var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");	
    var endTime = new Date("29 April 2019 9:56:00 GMT+01:00");
    endTime = (Date.parse(endTime) / 1000);
  
    var now = new Date();
    now = (Date.parse(now) / 1000);
  
    var timeLeft = endTime - now;
  
    var days = Math.floor(timeLeft / 86400);
    var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600)) / 60);
    var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
    if (hours < "10") {
      hours = "0" + hours;
    }
    if (minutes < "10") {
      minutes = "0" + minutes;
    }
    if (seconds < "10") {
      seconds = "0" + seconds;
    }
  
    $("#hours").html("<span class='big'>"+ hours +"</span><span class='small'>HOURS</span>");
    $("#minutes").html("<span class='big'>"+ minutes +"</span><span class='small'>MIN</span>");
    $("#seconds").html("<span class='big'>"+ seconds +"</span><span class='small'>SEC</span>");
  }
  
  setInterval(function () {
    makeTimer();
  }, 1000);
  
  